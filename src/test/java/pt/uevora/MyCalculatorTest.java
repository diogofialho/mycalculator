package pt.uevora;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class MyCalculatorTest {

    @BeforeEach
    public void setUp(){
        
    }

    @AfterEach
    public void down(){
    }

    @Test
    public void testSum() throws Exception {
      MyCalculator test = new MyCalculator();
      assertEquals(0,test.sum("1","-1"));
      assertEquals(2,test.sum("1","1"));
      assertEquals(1,test.sum("0","1"));
      assertEquals(-1,test.sum("-1","0"));
    }

    @Test
    public void testSub() throws Exception{
        MyCalculator test = new MyCalculator();
        assertEquals(0,test.sub("1","1"));
        assertEquals(-1,test.sub("0","1"));
        assertEquals(1,test.sub("0","-1"));
        assertEquals(0,test.sub("-1","-1"));
    }

    @Test
    public void testMult() throws Exception{
        MyCalculator test = new MyCalculator();
        assertEquals(0,test.mult("1","0"));
        assertEquals(1,test.mult("1","1"));
        assertEquals(6,test.mult("2","3"));
        assertEquals(-1,test.mult("1","-1"));
        assertEquals(2,test.mult("-2","-1"));
    }

    @Test
    public void testDiv() throws Exception{
        MyCalculator test = new MyCalculator();
        assertEquals(1,test.div("2","2"));
        assertEquals(0,test.div("0","1"));
        assertEquals(-2,test.div("-2","1"));
        assertEquals(1,test.div("-1","-1"));
        assertEquals(Double.POSITIVE_INFINITY,test.div("1","0"));
    }
}